﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Openwt.Contact.API.Model
{
    public class ContactIdSkillIdModel
    {
        [Required]
        public int ContactId { get; set; }
        [Required]
        public int SkillId { get; set; }
    }
}
