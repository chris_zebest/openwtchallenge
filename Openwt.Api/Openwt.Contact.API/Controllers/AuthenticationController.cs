﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Openwt.Contact.Domain.Contract;
using Openwt.Contact.Domain.Model;
using Openwt.Core;

namespace Openwt.Contact.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService, ILogger<AuthenticationController> logger)
        {
            this._logger = logger;
            this._authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [Route("signin")]
        [HttpPost]
        public async Task<IActionResult> Signin([FromBody]LoginModel model)
        {
            try
            {
                return Ok(await this._authenticationService.AuthenticateAsync(model.Username, model.Password));
            }
            catch (OpenwtException)
            {
                return this.BadRequest();
            }
        }
    }
}