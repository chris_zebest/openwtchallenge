﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Openwt.Contact.Domain.Model;
using Openwt.Contact.Domain.Service;

namespace Openwt.Contact.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SkillController : ControllerBase
    {
        private readonly ILogger<SkillController> _logger;
        private UserManager<IdentityUser> _userManager;
        private ISkillService _skillService;
        public SkillController( UserManager<IdentityUser> userManager,
                                ISkillService skillService,
                                ILogger<SkillController> logger)
        {
            this._skillService = skillService;
            this._userManager = userManager;
            this._logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SkillModel>>> Get()
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok(await this._skillService.AllAsync());
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
            
        }
    }
}