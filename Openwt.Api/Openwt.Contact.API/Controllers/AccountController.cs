﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Openwt.Contact;
using Openwt.Contact.Domain.Contract;
using Openwt.Contact.Domain.Model;
using Openwt.Core;

namespace Openwt.Contact.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private IUserService _userService;

        public AccountController(IUserService userService, ILogger<AccountController> logger)
        {
            this._logger = logger;
            this._userService = userService;
        }

        
        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public  async Task<IActionResult> RegisterAsync([FromBody]RegisterModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await this._userService.CreateAsync(model.Email, model.Password);
                }
            }
            catch(Exception e)
            {
                this._logger.LogWarning(e.ToString());
                return this.BadRequest();
            }
            return this.Ok();
        }

    }
}
