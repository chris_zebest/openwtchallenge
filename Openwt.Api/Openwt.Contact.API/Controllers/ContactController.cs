﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Openwt.Contact;
using Openwt.Contact.Domain.Model;
using Openwt.Contact.Domain.Service;

namespace Openwt.Contact.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class ContactController : ControllerBase
    {
        private readonly ILogger<ContactController> _logger;
        private UserManager<IdentityUser> _userManager;
        private IContactService _contactService;
        public ContactController(   UserManager<IdentityUser> userManager, 
                                    IContactService contactService,
                                    ILogger<ContactController> logger)
        {
            this._contactService = contactService;
            this._userManager = userManager;
            this._logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult<IEnumerable<ContactModel>>> Get()
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok(await this._contactService.AllAsync(userId));
            }
            catch( Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
        }

        [Route("details/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult<ContactModel>> Details(int id)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok(await this._contactService.ReadAsync(userId, id));
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
        }

        [Route("create")]
        [HttpPut]
        [HttpPost]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult<ContactModel>> Create([FromBody]ContactModel model)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                model.Id = await this._contactService.CreateAsync(model, userId);
                return this.Ok(model);
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
        }

        [Route("edit")]
        [HttpPost]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult<ContactModel>> Edit([FromBody]ContactModel model)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok(await this._contactService.UpdateAsync(model, userId));
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                await this._contactService.DeleteAsync(userId, id);
                return this.Ok();
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
        }
    }
}
