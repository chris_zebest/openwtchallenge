﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Openwt.Contact.API.Model;
using Openwt.Contact.Domain.Model;
using Openwt.Contact.Domain.Service;

namespace Openwt.Contact.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ContactSkillController : ControllerBase
    {
        private readonly ILogger<ContactSkillController> _logger;
        private UserManager<IdentityUser> _userManager;
        private IContactSkillService _contactSkillService;
        public ContactSkillController(UserManager<IdentityUser> userManager,
                                        IContactSkillService contactSkillService,
                                        ILogger<ContactSkillController> logger)
        {
            this._contactSkillService = contactSkillService;
            this._userManager = userManager;
            this._logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult<IEnumerable<ContactSkillModel>>> Get()
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok(await this._contactSkillService.AllAsync(userId));
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }

        }


        [Route("details/{id}")]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [HttpGet]
        public async Task<ActionResult<ContactSkillModel>> Details(int id)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok(await this._contactSkillService.ReadAsync(userId, id));
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }

        }

        [Route("create")]
        [HttpPut]
        [HttpPost]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult> Create([FromBody]ContactIdSkillIdModel requestModel)
        {
            int id = 0;
            try
            {
                var userId = this._userManager.GetUserId(User);
                var model = new ContactSkillModel { ContactId = requestModel.ContactId, Skill = new SkillModel { Id = requestModel.SkillId } };
                id = await this._contactSkillService.CreateAsync(model, userId);
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
            return this.Ok(new { Id = id });

        }

        [Route("edit")]
        [HttpPost]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult<ContactSkillModel>> Edit([FromBody]ContactSkillModel model)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                return this.Ok( await this._contactSkillService.UpdateAsync(model, userId));
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
        }
        [Route("delete/{id}")]
        [HttpDelete]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var userId = this._userManager.GetUserId(User);
                await this._contactSkillService.DeleteAsync(userId, id);
            }
            catch (Exception e)
            {
                this._logger.LogError(e.ToString());
                return this.BadRequest();
            }
            return this.Ok();

        }
    }
}