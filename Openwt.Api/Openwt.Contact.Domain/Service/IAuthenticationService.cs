﻿using Openwt.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Domain.Contract
{
    public interface IAuthenticationService
    {
        Task<AuthToken> AuthenticateAsync(string username, string password);
    }
}
