﻿using Openwt.Contact.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Domain.Service
{
    public interface IContactService
    {
        public Task<IList<ContactModel>> AllAsync(string userId);
        public Task<ContactModel> ReadAsync(string userId,int contactId);
        public Task<int> CreateAsync(ContactModel model, string userId);
        public Task<ContactModel> UpdateAsync(ContactModel model, string userId);
        public Task DeleteAsync(string userId, int contactId);
    }
}
