﻿using Openwt.Contact.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Domain.Service
{
    public interface ISkillService
    {
        public Task<IList<SkillModel>> AllAsync();
    }
}
