﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Domain.Contract
{
    public interface IUserService
    {
        public Task CreateAsync(string email, string password);
    }
}
