﻿using Openwt.Contact.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Domain.Service
{
    public interface IContactSkillService
    {
        public Task<IList<ContactSkillModel>> AllAsync(string userId);
        public Task<ContactSkillModel> ReadAsync(string userId,int contactSkillId);
        public Task<int> CreateAsync(ContactSkillModel model, string userId);
        public Task<ContactSkillModel> UpdateAsync(ContactSkillModel model , string userId);
        public Task DeleteAsync(string userId, int contactSkillId);
    }
}
