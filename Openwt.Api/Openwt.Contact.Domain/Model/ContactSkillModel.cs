﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Openwt.Contact.Domain.Model
{
    public class ContactSkillModel
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public SkillModel Skill { get; set; }
    }
}
