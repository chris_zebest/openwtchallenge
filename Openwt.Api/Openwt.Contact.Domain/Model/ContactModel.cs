﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Openwt.Contact.Domain.Model
{
    public class ContactModel
    {
        public int Id { get; set; }
        [Required]
        public string Lastname { get; set; }
        [Required]
        public string Firstname { get; set; }
        public string Fullname{ get { return $"{this.Firstname} {this.Lastname}"; } }
        public string Address { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string MobilePhone { get; set; }
    }
}
