﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Openwt.Contact.Domain.Model
{
    public class SkillModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
