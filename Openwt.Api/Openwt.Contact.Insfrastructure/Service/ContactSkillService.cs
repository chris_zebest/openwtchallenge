﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Openwt.Contact.Domain.Model;
using Openwt.Contact.Domain.Service;
using Openwt.Contact.Insfrastructure.Data;
using Openwt.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Insfrastructure.Service
{
    public class ContactSkillService : IContactSkillService
    {
        private ContactDbContext _contactDbContext;
        public ContactSkillService(ContactDbContext contactDbContext)
        {
            this._contactDbContext = contactDbContext;
        }
        public async Task<IList<ContactSkillModel>> AllAsync(string userId)
        {
            return await this._contactDbContext.ContactSkill
                .Where(contactSkill => contactSkill.Contact.UserId == userId)
                .Select(contactSkill => new ContactSkillModel
                {
                    Id = contactSkill.Id,
                    ContactId = contactSkill.ContactId,
                    Skill = new SkillModel
                    {
                        Id = contactSkill.Skill.Id,
                        Name = contactSkill.Skill.Name
                    }
                }).ToListAsync();
        }
        public async Task<ContactSkillModel> ReadAsync(string userId, int contactSkillId)
        {
            if (contactSkillId == 0)
            {
                throw new ArgumentException("contactSkillId");
            }
            return await this._contactDbContext.ContactSkill
                                    .Where(contactSkill => contactSkill.Id == contactSkillId && contactSkill.Contact.UserId == userId)
                                    .Select(contactSkill =>
                                       new ContactSkillModel
                                       {
                                           Id = contactSkill.Id,
                                           ContactId = contactSkill.ContactId,
                                           Skill = new SkillModel
                                           {
                                               Id = contactSkill.SkillId,
                                               Name = contactSkill.Skill.Name
                                           }
                                       })
                                    .FirstOrDefaultAsync();

        }
        public async Task<int> CreateAsync(ContactSkillModel model, string userId)
        {
            if(model==null)
            {
                throw new ArgumentNullException("model");
            }
            if (model.ContactId == 0)
            {
                throw new ArgumentException("model.contactId");
            }
            if (model.Skill == null)
            {
                throw new ArgumentNullException("model.Skill");
            }
            if (model.Skill.Id == 0)
            {
                throw new ArgumentException("model.Skill.Id");
            }
            var contact = await this._contactDbContext.Contact.FindAsync(model.ContactId);
            if (contact == null || contact.UserId != userId)
            {
                throw new OpenwtException($"User:{userId} cannot edit contact: {model.ContactId}");
            }

            var contactSkill = new Data.ContactSkill
            {
                ContactId = model.ContactId,
                SkillId = model.Skill.Id
            };
            this._contactDbContext.ContactSkill.Add(contactSkill);
            int rowCount = await this._contactDbContext.SaveChangesAsync();
            if (rowCount != 1)
            {
                throw new OpenwtException($"Inserted rowCount={rowCount} model={JsonConvert.SerializeObject(new { ContactId = model.ContactId, SkillId = model.Skill.Id })}");
            }
            return contactSkill.Id;
        }

        public async Task<ContactSkillModel> UpdateAsync(ContactSkillModel model, string userId)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            if (model.Id == 0)
            {
                throw new ArgumentException("model.Id");
            }
            if (model.Skill == null)
            {
                throw new ArgumentNullException("model.Skill");
            }
            if (model.Skill.Id == 0)
            {
                throw new ArgumentException("model.Skill.Id");
            }

            var contactSkillData = await this._contactDbContext.ContactSkill
                                .Where(contactSkill => contactSkill.Id == model.Id && contactSkill.Contact.UserId == userId)
                                .FirstOrDefaultAsync();
            if (contactSkillData == null)
            {
                throw new OpenwtException($"User:{userId} cannot update contactSkill: {model.Id}");
            }

            contactSkillData.SkillId = model.Skill.Id;
            contactSkillData.ContactId = model.ContactId;
            int rowCount = await this._contactDbContext.SaveChangesAsync();
            if (rowCount != 1)
            {
                throw new OpenwtException($"Updated rowCount={rowCount} model={JsonConvert.SerializeObject(model)}");
            }
            return model;

        }
        public async Task DeleteAsync(string userId, int contactSkillId)
        {
            if (contactSkillId == 0)
            {
                throw new ArgumentException("contactSkillId");
            }
            var contactSkillData = await this._contactDbContext.ContactSkill
                                .Where(contactSkill => contactSkill.Id == contactSkillId && contactSkill.Contact.UserId == userId)
                                .FirstOrDefaultAsync();
            if (contactSkillData == null)
            {
                throw new OpenwtException($"User:{userId} cannot delete contactSkill: {contactSkillId}");
            }
            this._contactDbContext.ContactSkill.Remove(contactSkillData);
            int rowCount = await this._contactDbContext.SaveChangesAsync();
            if (rowCount != 1)
            {
                throw new OpenwtException($"Deleted rowcount={rowCount} data={JsonConvert.SerializeObject(new { contactSkillId = contactSkillId, userId = userId })}");
            }
        }
    }
}
