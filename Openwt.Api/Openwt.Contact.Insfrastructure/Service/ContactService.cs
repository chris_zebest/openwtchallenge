﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Openwt.Contact.Domain.Model;
using Openwt.Contact.Domain.Service;
using Openwt.Contact.Insfrastructure.Data;
using Openwt.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Insfrastructure.Service
{
    public class ContactService : IContactService
    {
        private ContactDbContext _contactDbContext;
        public ContactService(ContactDbContext contactDbContext) 
        {
            this._contactDbContext = contactDbContext;
        }
        public async Task<IList<ContactModel>> AllAsync(string userId)
        {
            return await this._contactDbContext.Contact
                .Where(contact => contact.UserId == userId)
                .Select(contact=>new ContactModel
                {
                    Id= contact.Id,
                    Lastname= contact.Lastname,
                    Firstname= contact.Firstname,
                    Address = contact.Address,
                    MobilePhone = contact.Mobilephone,
                    Email = contact.Email
                }).ToListAsync();
        }
        public async Task<ContactModel> ReadAsync(string userId, int contactId)
        {
            var contact= await this._contactDbContext.Contact.FindAsync(contactId);
            if (contact == null || contact.UserId != userId)
            {
                throw new OpenwtException($"{userId} cannot get {contactId}");
            }
            return new ContactModel
            {
                Id = contact.Id,
                Firstname = contact.Firstname,
                Lastname = contact.Lastname,
                Address = contact.Address,
                MobilePhone = contact.Mobilephone,
                Email = contact.Email
            };
        }
        public async Task<int> CreateAsync(ContactModel model, string userId)
        {
            var contact = new Data.Contact
            {
                Firstname = model.Firstname,
                Lastname = model.Lastname,
                Address = model.Address,
                Mobilephone = model.MobilePhone,
                Email = model.Email,
                UserId = userId
            };
            this._contactDbContext.Contact.Add(contact);
            int rowCount = await this._contactDbContext.SaveChangesAsync();
            if (rowCount  != 1)
            {
                throw new OpenwtException($"Inserted rowCount={rowCount} model={JsonConvert.SerializeObject(model)}");
            }
            return contact.Id;
        }

        public async Task<ContactModel> UpdateAsync(ContactModel model, string userId)
        {
            if(model.Id==0)
            {
                throw new ArgumentException("model.Id");
            }
            var contact = await this._contactDbContext.Contact.FindAsync(model.Id);
            if (contact==null || contact.UserId != userId)
            {
                return new ContactModel();
            }
            contact.Firstname = model.Firstname;
            contact.Lastname = model.Lastname;
            contact.Address = model.Address;
            contact.Email = model.Email;
            contact.Mobilephone = model.MobilePhone;
            int rowCount = await this._contactDbContext.SaveChangesAsync();
            if (rowCount != 1)
            {
                throw new OpenwtException($"Updated rowCount={rowCount} model={JsonConvert.SerializeObject(model)}");
            }
            return model;

        }
        public async Task DeleteAsync(string userId, int contactId)
        {
            var contact = await this._contactDbContext.Contact.FindAsync(contactId);
            if (contact == null || contact.UserId != userId)
            {
                return;
            }
            this._contactDbContext.Contact.Remove(contact);
            int rowCount = await this._contactDbContext.SaveChangesAsync();
            if (rowCount != 1)
            {
                throw new OpenwtException($"Deleted rowcount={rowCount} data={JsonConvert.SerializeObject(new {contactId=contactId, userId=userId})}");
            }
        }
    }
}
