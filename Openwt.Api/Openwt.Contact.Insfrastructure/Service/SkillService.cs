﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Openwt.Contact.Domain.Contract;
using Openwt.Contact.Domain.Model;
using Openwt.Contact.Domain.Service;
using Openwt.Contact.Insfrastructure.Data;
using Openwt.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Insfrastructure.Service
{
    public class SkillService : ISkillService
    {
        private ContactDbContext _contactDbContext;
        public SkillService(ContactDbContext contactDbContext)
        {
            this._contactDbContext = contactDbContext;
        }

        public async Task<IList<SkillModel>> AllAsync()
        {
            return await this._contactDbContext.Skill
                                    .Select(skill =>
                                    new SkillModel
                                    {
                                        Id = skill.Id,
                                        Name = skill.Name
                                    }).ToListAsync();
        }
    }
}
