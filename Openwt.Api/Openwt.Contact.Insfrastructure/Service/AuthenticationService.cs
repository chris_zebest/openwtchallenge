﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Openwt.Contact.Domain.Contract;
using Openwt.Core;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Insfrastructure.Service
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly AuthenticationSettings _authenticationSettings;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AuthenticationService(   IOptions<AuthenticationSettings> authenticationSettings,
                                       UserManager<IdentityUser> userManager,
                                       SignInManager<IdentityUser> signInManager)
        {
            this._authenticationSettings = authenticationSettings.Value;
            this._userManager = userManager;
            this._signInManager = signInManager;
        }

        public async Task<AuthToken> AuthenticateAsync(string username, string password)
        {
            var user = await this._userManager.FindByNameAsync(username);
            if (user == null)
                return null;

            if (!(await this._signInManager.CanSignInAsync(user)))
                throw new OpenwtException();
            var result = await this._signInManager.CheckPasswordSignInAsync(user, password, true);
            if (!result.Succeeded)
                throw new OpenwtException();


            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(this._authenticationSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.Jti, user.UserName.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Email, user.UserName.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return new AuthToken { Token = tokenHandler.WriteToken(token) };
        }
    }
}
