﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Openwt.Contact.Domain.Contract;
using Openwt.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Openwt.Contact.Insfrastructure.Service
{
    public class UserService : IUserService
    {
        private UserManager<IdentityUser> _userManager;
        public UserService(UserManager<IdentityUser> userManager)
        {
            this._userManager = userManager;

        }
        public async Task CreateAsync(string email, string password)
        {
            var user = new IdentityUser(email);
            user.Email = email;
            user.EmailConfirmed = true;//Should be done through the validation email
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                throw new OpenwtException($"Error creating user {email}: { JsonConvert.SerializeObject(result.Errors)}");
                
            }
            //Send email...
            //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            //...

        }
    }
}
