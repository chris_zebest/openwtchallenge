﻿using System;
using System.Collections.Generic;

namespace Openwt.Contact.Insfrastructure.Data
{
    public partial class Skill
    {
        public Skill()
        {
            ContactSkill = new HashSet<ContactSkill>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ContactSkill> ContactSkill { get; set; }
    }
}
