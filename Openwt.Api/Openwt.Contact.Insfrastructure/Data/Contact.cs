﻿using System;
using System.Collections.Generic;

namespace Openwt.Contact.Insfrastructure.Data
{
    public partial class Contact
    {
        public Contact()
        {
            ContactSkill = new HashSet<ContactSkill>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Mobilephone { get; set; }
        public string UserId { get; set; }

        public virtual ICollection<ContactSkill> ContactSkill { get; set; }
    }
}
