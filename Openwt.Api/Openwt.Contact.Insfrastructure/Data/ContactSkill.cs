﻿using System;
using System.Collections.Generic;

namespace Openwt.Contact.Insfrastructure.Data
{
    public partial class ContactSkill
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public int SkillId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
