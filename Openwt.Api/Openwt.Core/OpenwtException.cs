﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Openwt.Core
{
    public class OpenwtException : Exception
    {
        public OpenwtException(){}
        public OpenwtException(string? message):base(message) { }
        public OpenwtException(string? message, Exception? innerException) : base(message, innerException) { }
        public OpenwtException(SerializationInfo info, StreamingContext context):base(info, context) { }
    }
}
